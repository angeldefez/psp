#!/usr/bin/env python

import sys, getopt

def ayuda():
    print('Usa: python ejercicio2.py -f|--file <archivo>')
    sys.exit(2) # salimos con error

def parametros():
    try:
      opts, args = getopt.getopt(sys.argv[1:], 'f:', ['file']) # la lista de parámetros que podemos usar
    except getopt.GetoptError: # si algún parámetro no incluye su opción requerida...
      ayuda() # mostramos un mensaje de ayuda
    
    for opt, arg in opts:
        if opt in ('-f', '--file'):
            csv(arg)
        else:
            ayuda()

def limpia_linea(linea):
    return linea.replace('"','').replace('\n','').split(',') # limpiamos caracteres no deseados y dividimos la línea en un array

def csv(archivo):
    print ('NOMBRES DE COMPAÑÍAS:')
    with open(archivo, 'r') as f: # abrirmos para sólo lectura (binario)
        dict = {}
        cabecera = limpia_linea(f.readline()) # nos guardamos la primera línea como cabecera en un array
        for linea in f:
            l = limpia_linea(linea)
            nombre = l[3]
            dict[nombre] = 0
        printDict(dict)

        print ('AHORA MOSTRAMOS SUS TOTALES:')
        
        f.seek(0) # nos situamos al principio del fichero
        cabecera = limpia_linea(f.readline()) # nos guardamos la primera línea como cabecera en un array
        for linea in f:
            l = limpia_linea(linea)
            nombre = l[3]
            valor = l[4]
            dict[nombre] += int(valor)
        f.close() # cerramos el archivo
    printDict(dict)

def printDict(d):
    for i in d:
        print(f'-{i}: {d[i]}')

if __name__ == "__main__":
    parametros()