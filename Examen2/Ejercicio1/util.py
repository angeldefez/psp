from cryptography.fernet import Fernet
import sys, getopt, os

def encode(texto, key):
    f = Fernet(key) # creamos el objeto Fenet
    token = f.encrypt(texto) # convertimos el texto de string a binario antes de generar el token
    #print(f'texto\n>>> encoded >>>\n{token.decode()}') # mostramos el token codificado
    return(token)

def decode(texto, key):
    f = Fernet(key) # creamos el objeto Fenet
    token = f.decrypt(texto) # convertimos el texto de string a binario antes de generar el token
    #print(f'{token.decode()}\n<<< decoded <<<\n{texto}') # mostramos el resultado
    return(token)