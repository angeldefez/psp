#!/usr/bin/env python

import threading 
from tkinter import *

key = None # esta será nuestra clave

# importamos la configuración del servidor (SERVER, PORT)
from config import *
from util import *

client = socket.socket(socket.AF_INET, socket.SOCK_STREAM) # creamos un nuevo shocket de cliente

def conectar(server):
	# guardamos la dirección como una tupla
	ADDRESS = (server, PORT)
	client.connect(ADDRESS) # nos conectamos al servidor

# GUI class for the chat 
class GUI: 
	# método constructor 
	def __init__(self): 
		
		# ventana de chat (inicialmente oculta) 
		self.Window = Tk() 
		self.Window.withdraw() 
		
		# ventana de login 
		self.login = Toplevel() 
		# definimos el título 
		self.login.title("Login") 
		self.login.resizable(width = False, 
							height = False) 
		self.login.configure(width = 400, 
							height = 300) 
		# etiqueta 
		self.pls = Label(self.login, 
					text = "Loguéate para continuar", 
					justify = CENTER, 
					font = "Helvetica 14 bold") 
		
		self.pls.place(relheight = 0.15, 
					relx = 0.2, 
					rely = 0.07) 
		# etiqueta 
		self.labelName = Label(self.login, 
							text = "Nombre: ", 
							font = "Helvetica 12") 
		
		self.labelName.place(relheight = 0.2, 
							relx = 0.15, 
							rely = 0.2) 
		
		# creamos un cuadro de texto para introducir el nombre
		self.entryName = Entry(self.login, 
							font = "Helvetica 14") 
		
		self.entryName.place(relwidth = 0.4, 
							relheight = 0.12, 
							relx = 0.35, 
							rely = 0.2)
		
		# colocamos el foco en el cuadro de texto
		self.entryName.focus() 
		
		# creamos un cuadro de texto para introducir el servidor
		self.entryServidor = Entry(self.login, 
							font = "Helvetica 13")
		self.entryServidor.insert(END, SERVER) # añadimos de texto la dirección del servidor
		
		self.entryServidor.place(relwidth = 0.4, 
							relheight = 0.12, 
							relx = 0.35, 
							rely = 0.35)
		
		# definimos una etiqueta para el servidor
		self.labelServidor = Label(self.login, 
							text = "Servidor: ", 
							font = "Helvetica 12") 
		
		self.labelServidor.place(relheight = 0.2, 
							relx = 0.15, 
							rely = 0.35) 
		
		# creamos un botón y le asociamos una acción
		self.go = Button(self.login, 
						text = "CONTINUAR", 
						font = "Helvetica 14 bold", 
						command = lambda: self.goAhead(self.entryName.get())) 
		
		self.go.place(relx = 0.4, 
					rely = 0.55)
		
		self.Window.mainloop() 

	def goAhead(self, name): 
		server = self.entryServidor.get() # recogemos la dirección del servidor del cuadro de texto
		
		try:
			conectar(server) # intentamos conectar con el servidor, con la IP dada
			self.login.destroy() # ocultamos la ventana de login
			self.layout(name) # mostramos la siguiente ventana
		
			# iniciamos el hilo para recibir mensajes
			rcv = threading.Thread(target=self.receive) 
			rcv.start() 
		except socket.error: # si hay algún error de conexión...
			# creamos etiqueta de error
			self.error = Label(self.login, 
						text = "Error al conectar con el servidor,\n inténtalo de nuevo", 
						justify = CENTER,
						fg = '#F33',
						font = "Helvetica 12 bold") 
			
			self.error.place(relheight = 0.15, 
						relx = 0.15, 
						rely = 0.70) 

	# el layout principal de la ventana de chat 
	def layout(self,name): 
		
		self.name = name 
		# mostramos la ventana del chat 
		self.Window.deiconify() 
		self.Window.title("CHAT ROOM") 
		self.Window.resizable(width = False, 
							height = False) 
		self.Window.configure(width = 470, 
							height = 550, 
							bg = "#17202A") 
		self.labelHead = Label(self.Window, 
							bg = "#17202A", 
							fg = "#EAECEE", 
							text = self.name , 
							font = "Helvetica 13 bold", 
							pady = 5) 
		
		self.labelHead.place(relwidth = 1) 
		self.line = Label(self.Window, 
						width = 450, 
						bg = "#ABB2B9") 
		
		self.line.place(relwidth = 1, 
						rely = 0.07, 
						relheight = 0.012) 
		
		self.textCons = Text(self.Window, 
							width = 20, 
							height = 2, 
							bg = "#17202A", 
							fg = "#EAECEE", 
							font = "Helvetica 14", 
							padx = 5, 
							pady = 5) 
		
		self.textCons.place(relheight = 0.745, 
							relwidth = 1, 
							rely = 0.08) 
		
		self.labelBottom = Label(self.Window, 
								bg = "#ABB2B9", 
								height = 80) 
		
		self.labelBottom.place(relwidth = 1, 
							rely = 0.825) 
		
		self.entryMsg = Entry(self.labelBottom, 
							bg = "#2C3E50", 
							fg = "#EAECEE", 
							font = "Helvetica 13") 
		
		self.entryMsg.place(relwidth = 0.74, 
							relheight = 0.06, 
							rely = 0.008, 
							relx = 0.011) 
		
		self.entryMsg.focus() 
		
		# creamos un botón de enviar 
		self.buttonMsg = Button(self.labelBottom, 
								text = "Send", 
								font = "Helvetica 10 bold", 
								width = 20, 
								bg = "#ABB2B9", 
								command = lambda : self.sendButton(self.entryMsg.get())) 
		
		self.buttonMsg.place(relx = 0.77, 
							rely = 0.008, 
							relheight = 0.06, 
							relwidth = 0.22) 
		
		self.textCons.config(cursor = "arrow") 
		
		# creamos una barra lateral de scroll 
		scrollbar = Scrollbar(self.textCons) 
		
		# colocamos la barra lateral
		scrollbar.place(relheight = 1, 
						relx = 0.974) 
		
		scrollbar.config(command = self.textCons.yview) 
		
		self.textCons.config(state = DISABLED) 

	# función para iniciar el hilo de enviar mensajes 
	def sendButton(self, msg): 
		self.textCons.config(state = DISABLED) 
		self.msg=msg 
		self.entryMsg.delete(0, END) 
		snd= threading.Thread(target = self.sendMessage) 
		snd.start() 

	# función para recibir mensajes 
	def receive(self): 
		while True: 
			try: 
				message = client.recv(1024).decode(FORMAT)
				message = decode(message,key)
				
				# si el mensaje del servidor es NOMBRE, envía el nombre del cliente
				if message == 'NOMBRE': 
					client.send(encode(self.name.encode(FORMAT), key))
				else: 
					# insertamos los mensajes en el text box 
					self.textCons.config(state = NORMAL) 
					self.textCons.insert(END, 
										message+"\n\n") 
					
					self.textCons.config(state = DISABLED)
					#self.textCons.config(foreground="red")

					self.textCons.see(END) 
			except: 
				print("Algo ha salido mal al recibir el mensaje") # si aparece algún error, mostramos un mensaje en la consola
				client.close() # cerramos la conexión
				break
		
	# función para enviar mensajes 
	def sendMessage(self): 
		self.textCons.config(state=DISABLED) 
		while True: 
			message = (f"{self.name}: {self.msg}")
			encript = encode(message.encode(FORMAT), key)
			client.send(encript)
			break	

def clave():
    global key
    
    if os.path.isfile(ARCHIVO): # si existe el archivo de password...
        with open(ARCHIVO, "rb") as a:
            key = a.read()
            a.close() # cerramos el archivo

if __name__ == "__main__":
	clave() # comprobamos si existe la clave
	if key is not None:
		g = GUI() # creamos un objeto de la clase GUI 
	else:
		print("No se ha encontrado el fichero de claves.")