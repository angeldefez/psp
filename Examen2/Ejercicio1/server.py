#!/usr/bin/env python

import socket 
import threading

# importamos la configuración del servidor (SERVER, PORT)
from config import *
from util import *

key = None # esta será nuestra clave

# guardamos la dirección como una tupla
ADDRESS = (SERVER, PORT) 

# nos creamos una lista de los clientes conectados y sus nombres
clients, names = [], [] 

# creamos un nuevo socket para el servidor
server = socket.socket(socket.AF_INET, socket.SOCK_STREAM) 

# asociamos la dirección del servidor al socket
server.bind(ADDRESS) 

# función para empezar la conexión 
def startChat(): 
	
	print(f'el server está ejecutándose en {SERVER}:{PORT}') 
	
	# empezamos a escuchar conexiones
	server.listen() 
	
	while True: 
		# aceptamos conexiones y devolvemos una nueva conexión al cliente con la dirección asociada a él
		conn, addr = server.accept() 
		# conn.send('NOMBRE'.encode(FORMAT))
		conn.send(encode('NOMBRE',key))
		
		name = conn.recv(1024).decode(FORMAT) # 1024 es el máximo de bytes de datos que pede recibir
		
		# añadimos el nombre y el cliente a sus respectivas listas
		names.append(name) 
		clients.append(conn) 
		
		print(f'Usuario: {name}') 
		
		# enviamos el mensaje a todos 
		#broadcastMessage(encode(f'{name} se ya unido al chat!',key).encode(FORMAT)) 
		#conn.send(encode('Conectado con éxito!',key).encode(FORMAT))
		
		# iniciamos el hilo para manejar los mensajes 
		thread = threading.Thread(target = handle, 
								args = (conn, addr)) 
		thread.start() 
		
		# número de clientes conectados
		print(f'conexiones activas {threading.activeCount()-1}') 

# método para manejar los mensajes entrantes 
def handle(conn, addr): 
	
	print(f'nueva conexión {addr}') 
	connected = True
	
	while connected: 
		# recive el mensaje 
		message = conn.recv(1024) 
		
		# reenvía el mensaje message 
		broadcastMessage(message) 
	
	# cerramos la conexión 
	conn.close()
	print(f'conexiones activas {threading.activeCount()-1}') 

# método para reenviar el mensaje a todos los cliantes
def broadcastMessage(message): 
	for client in clients: 
		client.send(message) 

def clave():
    global key
    
    if os.path.isfile(ARCHIVO): # si existe el archivo de password...
        with open(ARCHIVO, "rb") as a:
            key = a.read()

            a.close() # cerramos el archivo
    else: # si no...
        key = Fernet.generate_key() # creamos una key aleatoria
        with open(ARCHIVO, "wb") as a: # abrimos el archivo en modo binario
            a.write(key) # guardamos la clave
            a.close() # cerramos el archivo

if __name__ == "__main__":
	clave() # recuperamos la clave guardada o generamos una
	startChat() # llamamos al método para iniciar la comunicación
