import multiprocessing
import math

def sqrt(lista, nombre):
    result = []

    for n in lista:
        result.append(math.sqrt(n))
    print(f'Resultado de {nombre}: {result} = {sum(result)}')


if __name__ == '__main__':
    mylist = [2,4,9,16,25,36,49,64,81,100]

    sqrt(mylist, 'main')

    p1 = multiprocessing.Process(target=sqrt, args=(mylist,'proceso p1'))
    p1.start()
    p1.join()

    print('El proceso p1 está', 'vivo' if p1.is_alive() else 'muerto')
