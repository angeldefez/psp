import multiprocessing
import math

def sqrt(lista, result):
    for n in lista:
        result.append(math.sqrt(n))
    print(f'Resultado de {multiprocessing.current_process().pid}: {result} = {sum(result)}')


if __name__ == '__main__':
    mylist = [2,4,9,16,25,36,49,64,81,100]

    with multiprocessing.Manager() as manager:
        result = manager.list()  # <-- can be shared between processes.

        p1 = multiprocessing.Process(target=sqrt, args=(mylist, result,))
        p2 = multiprocessing.Process(target=sqrt, args=(mylist, result,))
        p1.start()
        p2.start()

        p1.join()
        p2.join()