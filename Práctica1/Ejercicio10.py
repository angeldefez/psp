def es_bisiesto(año):
    return (año % 4 == 0) or (año % 100 != 0) and (año % 400 == 0)

for año in range(1900,2021):
    bisiesto = '' if es_bisiesto(año) else 'no '
    print(f'El año {año} {bisiesto}es bisiesto')