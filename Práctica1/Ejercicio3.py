def filtrar_palabras(lista, n):
    array = []
    for palabra in lista:
        if len(palabra) > n:
            array.append(palabra)
    return array

lista = ["zanguango","onomatopeya","ababol","garrote","mediterráneo"]
print(filtrar_palabras(lista, 7))