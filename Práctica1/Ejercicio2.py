def mas_larga(lista):
    larga = ''
    for palabra in lista:
        if len(palabra) > len(larga):
            larga = palabra
    return larga

lista = ["zanguango","onomatopeya","ababol","garrote","mediterráneo"]
print(mas_larga(lista))