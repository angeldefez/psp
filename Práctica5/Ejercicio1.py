#!/usr/bin/env python

from cryptography.fernet import Fernet
import sys, getopt, os

key = ''
archivo = 'password'

def parametros():
    try:
      opts, args = getopt.getopt(sys.argv[1:],'e:d:') # la lista de parámetros que podemos usar
    except getopt.GetoptError: # si algún parámetro no incluye su opción requerida...
        print('Usa: Ejercicio1.py [-e|-d] <cadena de caracteres>') # mostramos un mensaje de ayuda
        sys.exit(2) # salimos con error

    for opt, arg in opts:
        if opt == '-e':
            encode(arg)
        elif opt == '-d':
            decode(arg)

def encode(texto):
    f = Fernet(key) # creamos el objeto Fenet
    token = f.encrypt(texto.encode()) # convertimos el texto de string a binario antes de generar el token
    print(f'texto\n>>> encoded >>>\n{token.decode()}') # mostramos el token codificado

def decode(texto):
    f = Fernet(key) # creamos el objeto Fenet
    token = f.decrypt(texto.encode()) # convertimos el texto de string a binario antes de generar el token
    print(f'{token.decode()}\n<<< decoded <<<\n{texto}') # mostramos el resultado 

def clave():
    global key
    
    if os.path.isfile(archivo): # si existe el archivo de password...
        with open(archivo, "rb") as a:
            key = a.read()
            # print(key)
            a.close() # cerramos el archivo
    else: # si no...
        key = Fernet.generate_key() # creamos una key aleatoria
        # print(key) # la mostramos
        with open(archivo, "wb") as a: # abrimos el archivo en modo binario
            a.write(key) # guardamos la clave
            a.close() # cerramos el archivo

if __name__ == "__main__":
    clave()
    parametros()