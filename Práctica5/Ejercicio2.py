#!/usr/bin/env python

import sys

ruta = 'selled_policies.csv'

def leer(ultimo=False):
    with open(ruta, 'r') as f: # abrirmos para sólo lectura
        cabecera = limpia_linea(f.readline()) # nos guardamos la primera línea como cabecera en un array
        for linea in f:
            i=0
            for l in limpia_linea(linea):
                z
                i += 1
            print() # añadimos un salto de línea al final de todos los datos
        f.close()
        
    if ultimo: # sí hemos marcado la opción, guardamo la última línea
        return linea


def limpia_linea(linea):
    return linea.replace('"','').replace('\n','').split(',') # limpiamos caracteres no deseados y dividimos la línea en un array

def agregar():
    ultima_linea = leer(True) 
    with open(ruta, 'a') as f:
        f.write(ultima_linea) # agregamos la última línea devuelta al final del archivo
        print('* duplicada la última línea')
        f.close()

def guardar():
   with open(ruta, 'w') as f: # esta opción crea el archivo si no existe o lo deja en blanco
       print('* archivo',ruta,'sobreescrito')

def parametros():
    opt = ''.join(sys.argv[1:]) # si uso getopt tengo problemas para capturar la opción -r+

    if opt == '-r':
        leer()
    elif opt == '-r+':
        agregar()
    elif opt == '-w':
        guardar()
    else:
        print('Usa: Ejercicio2.py -r[+]|w')
        sys.exit(2) # salimos con error

if __name__ == "__main__":
    parametros()