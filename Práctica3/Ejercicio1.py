import threading
import time

def funcion():
    nombre = threading.current_thread().name
    print(f'[+] Hilo {nombre} iniciado')
    time.sleep(3)
    print(f'[-] Hilo {nombre} terminado')

if __name__ == '__main__':
    hilos=[]
    for i in range(20):
        hilos.append(threading.Thread(target=funcion, name=i+1))
        hilos[i].start()
    
    for h in hilos:
        h.join()

    print('Terminado')