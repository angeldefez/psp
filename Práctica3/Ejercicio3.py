import threading
from threading import Lock
import time

def beber():
    global lock
    nombre = threading.current_thread().name

    lock.acquire()
    time.sleep(1)
    lock.release()
    print(f'[+] Hilo {nombre}... está satisfecho')

if __name__ == '__main__':
    lock = Lock()
    hilos=[]

    for i in range(20):
        hilos.append(threading.Thread(target=beber, name=i+1))
        hilos[i].start()
    
    for h in hilos:
        h.join()
    
    print("Terminado")
