import threading
from threading import Lock, Condition
import time
from random import randint

def inicio(cond):
    nombre = threading.current_thread().name
    print(f'[+] En hilo {nombre}')

    with cond:
        cond.wait
        while not cond.acquire:      
            time.sleep(0.5)
        cond.acquire
        beber(cond)
        cond.release



def beber(cond):
    nombre = threading.current_thread().name
    s = randint(1,10)
    with cond:
        print(f'[+] Bebiendo hilo {nombre}... espérate {s} segundos')
        time.sleep(s)
        print(f'[-] Estaba de muerte, hilo --{nombre}--')
        cond.notify

if __name__ == '__main__':
    cond = Condition()
    hilos=[]

    for i in range(20):
        hilos.append(threading.Thread(target=inicio, name=i+1, args=(cond,)))
        hilos[i].start()
    
    cond.notifyAll

    for h in hilos:
        h.join()
    
    print('[*] Terminado')
