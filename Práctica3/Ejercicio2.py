import threading
from threading import Semaphore
import time

def tunel():
    global sem
    nombre = threading.current_thread().name
    print(f'[+] Hilo {nombre}')

    time.sleep(3)
    sem.acquire()
    print(f'[▲] Hilo {nombre}... ha entrado en el túnel')
    
    time.sleep(1)
    sem.release()
    print(f'[▼] Hilo {nombre}... ha salido del túnel')

if __name__ == '__main__':
    sem = Semaphore(1) # no he entendido bien si el ejercicio debía de procesar más de 1 hilo por semáforo o no
    hilos=[]

    for i in range(20):
        hilos.append(threading.Thread(target=tunel, name=i+1))
        hilos[i].start()
    
    for h in hilos:
        h.join()

    print('Terminado')