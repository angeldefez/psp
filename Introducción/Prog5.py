numeros = [0,0]

for i in range(2):
    numeros[i] = int(input('Escribe un número: '))

for n in numeros:
    print('El número',n,'es','par' if n % 2 == 0 else 'impar')