numero = int(input('Escribe un número mayor que 0: '))

if numero > 0:
    for n in range(int(numero/2)):
        if numero % (n+1) == 0:
            print(n+1)
    print(numero)
else:
    print('El número no es mayor que 0')