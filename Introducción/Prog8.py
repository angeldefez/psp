palabras = int(input('¿Cuantas palabras quieres introducir?: '))
lista = []

for i in range(palabras):
    lista += [input(f'Escribe la palabra nº{i+1}: ')]

palabra = input("Escribe la palabra a buscar: ")

n = 0
for p in lista:
    if p == palabra:
        n = n +1
print(f'La palabra {palabra} aparece {n} veces')