palabras = int(input('¿Cuantas palabras quieres introducir?: '))
lista = ['']*palabras

for i in range(palabras):
    lista[i] = input(f'Escribe la palabra nº{i+1}: ')

print("Has introducido las siguientes palabras:")
for palabra in lista:
    print(palabra)