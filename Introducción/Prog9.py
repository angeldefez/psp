def divisor(numero, n):
    return numero % (n) == 0

def divisores(numero):
    if numero > 0:
        for n in range(int(numero/2)):
            if divisor(numero, n+1):
                print(n+1)
        print(numero)
    else:
        print('El número no es mayor que 0')

numero = int(input('Escribe un número mayor que 0: '))
divisores(numero)