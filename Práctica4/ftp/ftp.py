#!/usr/bin/env python

from ftplib import FTP, all_errors
import sys, getopt

def ayuda():
    print('Usa: fpt.py -l -d <directorio> -f <archivo>')
    sys.exit(2) # salimos con error

def parametros():
    directorio = ''
    archivo = ''

    try:
      opts, args = getopt.getopt(sys.argv[1:],'ld:f:') # la lista de parámetros que podemos usar
    except getopt.GetoptError: # si algún parámetro no incluye su opción requerida...
      ayuda() # mostramos un mensaje de ayuda
    
    for opt, arg in opts:
        if opt == '-l':
            ftp.dir() # listamos los directorios
        elif opt == '-d':
            directorio = arg
            if directorio in ftp.nlst(): # si ya existe el directorio...
                print(f'El directorio \'{directorio}\' ya existe') # lo indicamos
            else:
                ftp.mkd(dir) #si no, lo creamos
        elif opt == '-f':
            archivo = arg
            if directorio: # si hemos definido el nombre del directorio...
                ftp.cwd(directorio) # nos cambiamos al directorio definido
                file = open(archivo,'rb') # abrimos el archivo que vamos a enviar
                ftp.storlines(f'STOR {archivo}', file) # lo enviamos en modo ASCII
                ftp.retrlines('LIST') # mostramos la lista de archivos para ver si el archivo se ha subido
            else: # si no, mostramos la forma correcta de ejecutar el programa
                ayuda()

if __name__ == "__main__":
    ftp = FTP('macbuighome.no-ip.info') # creamos la conexión FTP
    ftp.set_pasv(False) # decimos que el modo de conexión será activo

    try:
        ftp.login('psp', '265nCwsEzG2Xv6XW') # intentamos conectarnos con el user y el pass
        print('Conexión OK') # mostramos que la conexión se ha realizado
        parametros() # pasamos a procesar los parámetros
    except all_errors as e: # capturamos las posibles excepciones
        print(e) # mostramos los posibles errores
    finally:
        ftp.quit() # cerramos la conexión FTP