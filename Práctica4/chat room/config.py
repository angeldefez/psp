# aquí guardaremos la configuración de conexión
import socket 

# elegimos un puerto libre
PORT = 5000
# obtenemos una dirección IPv4 para el servidor
SERVER = socket.gethostbyname(socket.gethostname()) # en este caso apunta a nuestra dirección loopback

# nuestra codificación de caracteres para codificar/decodificar los mensajes
FORMAT = "utf-8"